package sample.event.report.facade.event.listener.domain;

import sample.event.report.flow.productrecord.domain.logic.ProductRecordHandlerLogic;
import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.order.event.order.OrderCanceledEvent;
import sample.event.order.event.order.ProductSoldEvent;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class OrderDomainEventHandler {
    //
    private final ProductRecordHandlerLogic productRecordHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductSoldEvent event) {
        //
        productRecordHandlerLogic.onProductSold(event);
    }

    @DaysmanEventHandler
    public void handle(OrderCanceledEvent event) {
        //
        productRecordHandlerLogic.onOrderCanceled(event);
    }
}
