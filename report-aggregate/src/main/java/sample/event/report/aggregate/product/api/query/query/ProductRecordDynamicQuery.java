/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import sample.event.report.aggregate.product.store.maria.jpo.ProductRecordJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;

import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class ProductRecordDynamicQuery extends CqrsDynamicQuery<ProductRecord> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductRecordJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductRecordJpo.class);
        TypedQuery<ProductRecordJpo> query = RdbQueryBuilder.build(request);
        ProductRecordJpo productRecordJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(productRecordJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
