package sample.event.report.aggregate.product.domain.entity;

import sample.event.report.aggregate.product.domain.entity.sdo.ProductRecordCdo;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.drama.StageEntity;
import io.naradrama.prologue.domain.stage.ActorKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductRecord extends StageEntity implements DomainAggregate {
    //
    private String name;
    private String description;
    private String category;
    private String color;
    private int size;
    private String material;
    private int stockQuantity;
    private int soldQuantity;

    public ProductRecord(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public ProductRecord(ProductRecordCdo cdo) {
        //
        super(cdo.getId(), cdo.getActorKey());
        this.name = cdo.getName();
        this.description = cdo.getDescription();
        this.category = cdo.getCategory();
        this.color = cdo.getColor();
        this.material = cdo.getMaterial();
        this.stockQuantity = 0;
        this.soldQuantity = 0;
    }

    public static ProductRecord newInstance(ProductRecordCdo cdo, NameValueList nameValues) {
        //
        ProductRecord productRecord = new ProductRecord(cdo);
        productRecord.modifyAttributes(nameValues);

        return productRecord;
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {
        //
    }

    @Override
    public String toJson() {
        return super.toJson();
    }

    public static ProductRecord sample() {
        //
        return new ProductRecord(ProductRecordCdo.sample());
    }

//    public String getEntityId() {
//        //
//        return getId();
//    }
}
