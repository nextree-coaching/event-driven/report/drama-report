package sample.event.report.aggregate.product.domain.entity.sdo;

import io.naradrama.prologue.domain.stage.ActorKey;
import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductRecordCdo implements JsonSerializable {
    //
    private ActorKey actorKey;
    private String id;
    private String name;
    private String description;
    private String category;
    private String color;
    private int size;
    private String material;

    public String toString() {
        //
        return toJson();
    }

    public static ProductRecordCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ProductRecordCdo.class);
    }

    public static ProductRecordCdo sample() {
        //
        String productName = "dokdo belongs to korea";
        String productDescription = "텅 부분에 브랜드명을 새겨 넣어 브랜드 고유의 아이덴티티를 드러낸 모델로 소가죽, 고탁성 스판 소재로 출시되었습니다.";
        String productCategory = "Sneakers";
        String productColor = "#ff0000";
        String productMaterial = "fabric";
        int productSize = 250;

        return new ProductRecordCdo(ActorKey.sample(),
                UUID.randomUUID().toString(),
                productName,
                productDescription,
                productCategory,
                productColor,
                productSize,
                productMaterial
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }

}
