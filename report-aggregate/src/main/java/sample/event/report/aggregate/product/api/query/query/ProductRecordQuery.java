/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import sample.event.report.aggregate.product.store.ProductRecordStore;

@Getter
@Setter
@NoArgsConstructor
public class ProductRecordQuery extends CqrsBaseQuery<ProductRecord> {
    /* Autogen by nara studio */
    private String productRecordId;

    public void execute(ProductRecordStore productRecordStore) {
        /* Autogen by nara studio */
        setQueryResult(productRecordStore.retrieve(productRecordId));
    }
}
