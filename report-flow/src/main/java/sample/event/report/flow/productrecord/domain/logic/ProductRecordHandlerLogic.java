package sample.event.report.flow.productrecord.domain.logic;

import sample.event.report.aggregate.product.domain.entity.sdo.ProductRecordCdo;
import sample.event.report.aggregate.product.domain.logic.ProductRecordLogic;
import sample.event.report.aggregate.product.domain.logic.ProductRecordUserLogic;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sample.event.inventory.event.stock.ProductDeliveredEvent;
import sample.event.inventory.event.stock.ProductWarehousedEvent;
import sample.event.order.event.order.OrderCanceledEvent;
import sample.event.order.event.order.ProductSoldEvent;
import sample.event.product.event.product.ProductDomainEvent;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductRecordHandlerLogic {
    //
    private final ProductRecordLogic productRecordLogic;
    private final ProductRecordUserLogic productRecordUserLogic;

    public void onProduct(ProductDomainEvent event) {
        //
        log.debug("onProductEvent : \n{}", event.toString());
        switch (event.getEventType()) {
            case Registered:
                newRecord(event);
                break;
            case Modified:
                break;
            case Removed:
                break;
        }
    }

    private void newRecord(ProductDomainEvent event) {
        //
        ProductRecordCdo cdo = new ProductRecordCdo(event.getActorKey(),
                event.getId(),
                event.getName(),
                event.getDescription(),
                event.getCategory(),
                event.getColor(),
                event.getSize(),
                event.getCategory());
        productRecordLogic.registerProductRecord(cdo);
    }

    public void onProductSold(ProductSoldEvent event) {
        //
        productRecordUserLogic.increaseSoldQuantity(event.getProductId(), event.getQuantity());
        productRecordUserLogic.decreaseStockQuantity(event.getProductId(), event.getQuantity());
    }

    public void onOrderCanceled(OrderCanceledEvent event) {
        //
        productRecordUserLogic.decreaseSoldQuantity(event.getProductId(), event.getQuantity());
        productRecordUserLogic.increaseStockQuantity(event.getProductId(), event.getQuantity());
    }

    public void onProductWarehoused(ProductWarehousedEvent event) {
        //
        productRecordUserLogic.increaseStockQuantity(event.getProductId(), event.getIncreasedQuantity());
    }

    public void onProductDelivered(ProductDeliveredEvent event) {
        //
        productRecordUserLogic.decreaseStockQuantity(event.getProductId(), event.getDecreasedQuantity());
    }
}
