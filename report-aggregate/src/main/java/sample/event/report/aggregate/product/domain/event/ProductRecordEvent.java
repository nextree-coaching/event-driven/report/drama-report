/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductRecordEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private ProductRecord productRecord;
    private String productRecordId;
    private NameValueList nameValues;

    protected ProductRecordEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductRecordEvent newProductRecordRegisteredEvent(ProductRecord productRecord) {
        /* Autogen by nara studio */
        ProductRecordEvent event = new ProductRecordEvent(CqrsDataEventType.Registered);
        event.setProductRecord(productRecord);
        return event;
    }

    public static ProductRecordEvent newProductRecordModifiedEvent(String productRecordId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductRecordEvent event = new ProductRecordEvent(CqrsDataEventType.Modified);
        event.setProductRecordId(productRecordId);
        event.setNameValues(nameValues);
        return event;
    }

    public static ProductRecordEvent newProductRecordRemovedEvent(String productRecordId) {
        /* Autogen by nara studio */
        ProductRecordEvent event = new ProductRecordEvent(CqrsDataEventType.Removed);
        event.setProductRecordId(productRecordId);
        return event;
    }

    public static ProductRecordEvent newProductRecordRemovedEvent(ProductRecord productRecord) {
        /* Autogen by nara studio */
        ProductRecordEvent event = new ProductRecordEvent(CqrsDataEventType.Removed);
        event.setProductRecord(productRecord);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductRecordEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductRecordEvent.class);
    }
}
