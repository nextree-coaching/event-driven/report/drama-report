/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import sample.event.report.aggregate.product.store.maria.jpo.ProductRecordJpo;

public interface ProductRecordMariaRepository extends PagingAndSortingRepository<ProductRecordJpo, String> {
    /* Autogen by nara studio */
}
