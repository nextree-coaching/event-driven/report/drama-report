/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.query.rest;

import sample.event.report.aggregate.product.api.query.query.ProductRecordDynamicQuery;
import sample.event.report.aggregate.product.api.query.query.ProductRecordQuery;
import sample.event.report.aggregate.product.api.query.query.ProductRecordsDynamicQuery;
import sample.event.report.aggregate.product.store.ProductRecordStore;
import sample.event.report.aggregate.product.store.maria.jpo.ProductRecordJpo;
import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/aggregate/product/product-record/query")
public class ProductRecordQueryResource implements ProductRecordQueryFacade {
    //
    private final ProductRecordStore productRecordStore;
    private final RdbQueryRequest<ProductRecordJpo> request;

    public ProductRecordQueryResource(ProductRecordStore productRecordStore, EntityManager entityManager) {
        //
        this.productRecordStore = productRecordStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse execute(@RequestBody ProductRecordQuery productRecordQuery) {
        //
        productRecordQuery.execute(productRecordStore);
        return productRecordQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse execute(@RequestBody ProductRecordDynamicQuery productRecordDynamicQuery) {
        //
        productRecordDynamicQuery.execute(request);
        return productRecordDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse execute(@RequestBody ProductRecordsDynamicQuery productRecordsDynamicQuery) {
        //
        productRecordsDynamicQuery.execute(request);
        return productRecordsDynamicQuery.getQueryResponse();
    }
}
