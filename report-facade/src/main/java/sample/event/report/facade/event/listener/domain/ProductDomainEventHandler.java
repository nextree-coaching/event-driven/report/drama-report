package sample.event.report.facade.event.listener.domain;

import sample.event.report.flow.productrecord.domain.logic.ProductRecordHandlerLogic;
import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.product.event.product.ProductDomainEvent;

import javax.transaction.Transactional;


@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDomainEventHandler {
    //
    private final ProductRecordHandlerLogic productRecordHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductDomainEvent event) {
        //
        productRecordHandlerLogic.onProduct(event);
    }
}
