/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.command.rest;

import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import sample.event.report.aggregate.product.api.command.command.ProductRecordCommand;

public interface ProductFacade {
    /* Autogen by nara studio */
    CommandResponse executeProductRecord(ProductRecordCommand productRecordCommand);
}
