package sample.event.report.facade.event.listener.domain;

import sample.event.report.flow.productrecord.domain.logic.ProductRecordHandlerLogic;
import io.naraplatform.daysboy.event.DaysmanEventHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import sample.event.inventory.event.stock.ProductDeliveredEvent;
import sample.event.inventory.event.stock.ProductWarehousedEvent;

import javax.transaction.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class StockDomainEventHandler {
    //
    private final ProductRecordHandlerLogic productRecordHandlerLogic;

    @DaysmanEventHandler
    public void handle(ProductWarehousedEvent event) {
        //
        productRecordHandlerLogic.onProductWarehoused(event);
    }

    @DaysmanEventHandler
    public void handle(ProductDeliveredEvent event) {
        //
        productRecordHandlerLogic.onProductDelivered(event);
    }
}
