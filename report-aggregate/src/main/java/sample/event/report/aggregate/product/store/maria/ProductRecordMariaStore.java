/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.store.maria;

import org.springframework.stereotype.Repository;
import sample.event.report.aggregate.product.store.ProductRecordStore;
import sample.event.report.aggregate.product.store.maria.repository.ProductRecordMariaRepository;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import sample.event.report.aggregate.product.store.maria.jpo.ProductRecordJpo;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naradrama.prologue.domain.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class ProductRecordMariaStore implements ProductRecordStore {
    /* Autogen by nara studio */
    private final ProductRecordMariaRepository productRecordMariaRepository;

    public ProductRecordMariaStore(ProductRecordMariaRepository productRecordMariaRepository) {
        /* Autogen by nara studio */
        this.productRecordMariaRepository = productRecordMariaRepository;
    }

    @Override
    public void create(ProductRecord productRecord) {
        /* Autogen by nara studio */
        ProductRecordJpo productRecordJpo = new ProductRecordJpo(productRecord);
        productRecordMariaRepository.save(productRecordJpo);
    }

    @Override
    public ProductRecord retrieve(String id) {
        /* Autogen by nara studio */
        Optional<ProductRecordJpo> productRecordJpo = productRecordMariaRepository.findById(id);
        return productRecordJpo.map(ProductRecordJpo::toDomain).orElse(null);
    }

    @Override
    public void update(ProductRecord productRecord) {
        /* Autogen by nara studio */
        ProductRecordJpo productRecordJpo = new ProductRecordJpo(productRecord);
        productRecordMariaRepository.save(productRecordJpo);
    }

    @Override
    public void delete(ProductRecord productRecord) {
        /* Autogen by nara studio */
        productRecordMariaRepository.deleteById(productRecord.getEntityId());
    }

    @Override
    public void delete(String id) {
        //
        productRecordMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        //
        return productRecordMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        //
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
