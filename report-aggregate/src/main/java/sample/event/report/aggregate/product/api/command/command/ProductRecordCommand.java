/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import sample.event.report.aggregate.product.domain.entity.sdo.ProductRecordCdo;
import java.util.List;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductRecordCommand extends CqrsBaseCommand {
    //
    private ProductRecordCdo productRecordCdo;
    private List<ProductRecordCdo> productRecordCdos;
    private boolean multiCdo;
    private String productRecordId;
    private NameValueList nameValues;

    protected ProductRecordCommand(CqrsBaseCommandType type) {
        //
        super(type);
    }

    public static ProductRecordCommand newRegisterProductRecordCommand(ProductRecordCdo productRecordCdo) {
        //
        ProductRecordCommand command = new ProductRecordCommand(CqrsBaseCommandType.Register);
        command.setProductRecordCdo(productRecordCdo);
        return command;
    }

    public static ProductRecordCommand newRegisterProductRecordCommand(List<ProductRecordCdo> productRecordCdos) {
        //
        ProductRecordCommand command = new ProductRecordCommand(CqrsBaseCommandType.Register);
        command.setProductRecordCdos(productRecordCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static ProductRecordCommand newModifyProductRecordCommand(String productRecordId, NameValueList nameValues) {
        //
        ProductRecordCommand command = new ProductRecordCommand(CqrsBaseCommandType.Modify);
        command.setProductRecordId(productRecordId);
        command.setNameValues(nameValues);
        return command;
    }

    public static ProductRecordCommand newRemoveProductRecordCommand(String productRecordId) {
        //
        ProductRecordCommand command = new ProductRecordCommand(CqrsBaseCommandType.Remove);
        command.setProductRecordId(productRecordId);
        return command;
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductRecordCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, ProductRecordCommand.class);
    }

    public static ProductRecordCommand sampleForRegister() {
        //
        return newRegisterProductRecordCommand(ProductRecordCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sampleForRegister());
    }
}
