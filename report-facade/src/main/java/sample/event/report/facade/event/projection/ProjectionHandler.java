/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.facade.event.projection;

import sample.event.report.aggregate.product.domain.logic.ProductRecordLogic;
import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;
import sample.event.report.aggregate.product.domain.event.ProductRecordEvent;

public class ProjectionHandler {
    /* Autogen by nara studio */
    private final ProductRecordLogic productRecordLogic;

    public ProjectionHandler(ProductRecordLogic productRecordLogic) {
        /* Autogen by nara studio */
        this.productRecordLogic = productRecordLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "ProductRecordEvent":
                ProductRecordEvent productRecordEvent = ProductRecordEvent.fromJson(payload);
                productRecordLogic.handleEventForProjection(productRecordEvent);
                break;
        }
    }
}
