package sample.event.report.aggregate.product.domain.logic;

import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import sample.event.report.aggregate.product.store.ProductRecordStore;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductRecordUserLogic {
    //
    private final ProductRecordLogic productRecordLogic;
    private final ProductRecordStore productRecordStore;

    public int increaseStockQuantity(String productId, int quantity) {
        //
        ProductRecord productRecord = productRecordLogic.findProductRecord(productId);
        productRecord.setStockQuantity(productRecord.getStockQuantity() + quantity);
        productRecordStore.update(productRecord);

        return productRecord.getStockQuantity();
    }

    public int decreaseStockQuantity(String productId, int quantity) {
        //
        ProductRecord productRecord = productRecordLogic.findProductRecord(productId);
        productRecord.setStockQuantity(productRecord.getStockQuantity() - quantity);
        productRecordStore.update(productRecord);

        return productRecord.getStockQuantity();
    }

    public int increaseSoldQuantity(String productId, int quantity) {
        //
        ProductRecord productRecord = productRecordLogic.findProductRecord(productId);
        productRecord.setSoldQuantity(productRecord.getSoldQuantity() + quantity);
        productRecordStore.update(productRecord);

        return productRecord.getStockQuantity();
    }

    public int decreaseSoldQuantity(String productId, int quantity) {
        //
        ProductRecord productRecord = productRecordLogic.findProductRecord(productId);
        productRecord.setSoldQuantity(productRecord.getSoldQuantity() - quantity);
        productRecordStore.update(productRecord);

        return productRecord.getStockQuantity();
    }

}
