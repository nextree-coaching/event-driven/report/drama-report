/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.command.rest;

import sample.event.report.aggregate.product.api.command.command.ProductRecordCommand;
import sample.event.report.aggregate.product.domain.logic.ProductRecordLogic;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate/product")
public class ProductResource implements ProductFacade {
    /* Autogen by nara studio */
    private final ProductRecordLogic productRecordLogic;

    public ProductResource(ProductRecordLogic productRecordLogic) {
        /* Autogen by nara studio */
        this.productRecordLogic = productRecordLogic;
    }

    @Override
    @PostMapping("/product-record/command")
    public CommandResponse executeProductRecord(@RequestBody ProductRecordCommand productRecordCommand) {
        /* Autogen by nara studio */
        return productRecordLogic.routeCommand(productRecordCommand).getCommandResponse();
    }
}
