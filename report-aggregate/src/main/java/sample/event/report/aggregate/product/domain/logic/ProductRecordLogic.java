/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.domain.logic;

import io.naraplatform.daysboy.EventStream;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sample.event.report.aggregate.product.store.ProductRecordStore;
import sample.event.report.aggregate.product.api.command.command.ProductRecordCommand;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import java.util.Optional;
import sample.event.report.aggregate.product.domain.entity.sdo.ProductRecordCdo;
import sample.event.report.aggregate.product.domain.event.ProductRecordEvent;
import io.naradrama.prologue.domain.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;

import java.util.NoSuchElementException;

@Service
@Transactional
public class ProductRecordLogic {
    //
    private final ProductRecordStore productRecordStore;
    private final EventStream eventStreamService;

    public ProductRecordLogic(ProductRecordStore productRecordStore, EventStream eventStreamService) {
        //
        this.productRecordStore = productRecordStore;
        this.eventStreamService = eventStreamService;
    }

    public ProductRecordCommand routeCommand(ProductRecordCommand command) {
        //
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerProductRecords(command.getProductRecordCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerProductRecord(command.getProductRecordCdo(), nameValueList)).orElse(this.registerProductRecord(command.getProductRecordCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyProductRecord(command.getProductRecordId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getProductRecordId()));
                break;
            case Remove:
                this.removeProductRecord(command.getProductRecordId());
                command.setCommandResponse(new CommandResponse(command.getProductRecordId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerProductRecord(ProductRecordCdo productRecordCdo) {
        //
        ProductRecord productRecord = new ProductRecord(productRecordCdo);
        if (productRecordStore.exists(productRecord.getEntityId())) {
            throw new IllegalArgumentException("productRecord already exists. " + productRecord.getEntityId());
        }
        productRecordStore.create(productRecord);
        ProductRecordEvent productRecordEvent = ProductRecordEvent.newProductRecordRegisteredEvent(productRecord);
        eventStreamService.publishEvent(productRecordEvent);
        return productRecord.getEntityId();
    }

    public String registerProductRecord(ProductRecordCdo productRecordCdo, NameValueList nameValueList) {
        //
        ProductRecord productRecord = ProductRecord.newInstance(productRecordCdo, nameValueList);
        if (productRecordStore.exists(productRecord.getEntityId())) {
            throw new IllegalArgumentException("productRecord already exists. " + productRecord.getEntityId());
        }
        productRecordStore.create(productRecord);
        ProductRecordEvent productRecordEvent = ProductRecordEvent.newProductRecordRegisteredEvent(productRecord);
        eventStreamService.publishEvent(productRecordEvent);
        return productRecord.getEntityId();
    }

    public List<String> registerProductRecords(List<ProductRecordCdo> productRecordCdos) {
        //
        return productRecordCdos.stream().map(productRecordCdo -> this.registerProductRecord(productRecordCdo)).collect(Collectors.toList());
    }

    public ProductRecord findProductRecord(String productRecordId) {
        //
        ProductRecord productRecord = productRecordStore.retrieve(productRecordId);
        if (productRecord == null) {
            throw new NoSuchElementException("ProductRecord id: " + productRecordId);
        }
        return productRecord;
    }

    public void modifyProductRecord(String productRecordId, NameValueList nameValues) {
        //
        ProductRecord productRecord = findProductRecord(productRecordId);
        productRecord.modify(nameValues);
        productRecordStore.update(productRecord);
        ProductRecordEvent productRecordEvent = ProductRecordEvent.newProductRecordModifiedEvent(productRecordId, nameValues);
        eventStreamService.publishEvent(productRecordEvent);
    }

    public void removeProductRecord(String productRecordId) {
        //
        ProductRecord productRecord = findProductRecord(productRecordId);
        productRecordStore.delete(productRecord);
        ProductRecordEvent productRecordEvent = ProductRecordEvent.newProductRecordRemovedEvent(productRecord.getEntityId());
        eventStreamService.publishEvent(productRecordEvent);
    }

    public boolean existsProductRecord(String productRecordId) {
        //
        return productRecordStore.exists(productRecordId);
    }

    public void handleEventForProjection(ProductRecordEvent productRecordEvent) {
        //
        switch(productRecordEvent.getCqrsDataEventType()) {
            case Registered:
                productRecordStore.create(productRecordEvent.getProductRecord());
                break;
            case Modified:
                ProductRecord productRecord = productRecordStore.retrieve(productRecordEvent.getProductRecordId());
                productRecord.modify(productRecordEvent.getNameValues());
                productRecordStore.update(productRecord);
                break;
            case Removed:
                productRecordStore.delete(productRecordEvent.getProductRecordId());
                break;
        }
    }
}
