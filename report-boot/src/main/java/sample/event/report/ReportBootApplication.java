/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report;

import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import io.naradrama.prologue.util.rolekeeper.config.EnableDramaRoleBaseAccess;
import io.naradrama.prologue.util.spacekeeper.config.DramaApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.SpringApplication;

@EnableDramaRoleBaseAccess
@DramaApplication
@EnableSwagger2
@SpringBootApplication(scanBasePackages = { "sample.event.report", "io.naraplatform.daysboy.store" }, exclude = MongoAutoConfiguration.class)
@EnableJpaRepositories(basePackages = { "sample.event.report"})
@EntityScan(basePackages = { "sample.event.report.constant", "sample.event.report", "io.naraplatform.daysboy.store" })
public class ReportBootApplication {
    //

    public static void main(String[] args) {
        //
        SpringApplication.run(ReportBootApplication.class, args);
    }
}
