/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.api.query.rest;

import io.naradrama.prologue.domain.cqrs.query.QueryResponse;
import sample.event.report.aggregate.product.api.query.query.ProductRecordQuery;
import sample.event.report.aggregate.product.api.query.query.ProductRecordDynamicQuery;
import sample.event.report.aggregate.product.api.query.query.ProductRecordsDynamicQuery;

public interface ProductRecordQueryFacade {
    /* Autogen by nara studio */
    QueryResponse execute(ProductRecordQuery productRecordQuery);
    QueryResponse execute(ProductRecordDynamicQuery productRecordDynamicQuery);
    QueryResponse execute(ProductRecordsDynamicQuery productRecordsDynamicQuery);
}
