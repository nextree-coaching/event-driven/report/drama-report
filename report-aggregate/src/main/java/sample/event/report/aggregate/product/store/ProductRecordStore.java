/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.store;

import sample.event.report.aggregate.product.domain.entity.ProductRecord;

public interface ProductRecordStore {
    /* Autogen by nara studio */
    void create(ProductRecord productRecord);
    ProductRecord retrieve(String id);
    void update(ProductRecord productRecord);
    void delete(ProductRecord productRecord);
    void delete(String id);
    boolean exists(String id);
}
