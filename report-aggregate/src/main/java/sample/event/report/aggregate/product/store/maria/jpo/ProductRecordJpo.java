/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package sample.event.report.aggregate.product.store.maria.jpo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naradrama.prologue.store.jpa.StageEntityJpo;
import sample.event.report.aggregate.product.domain.entity.ProductRecord;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_RECORD")
public class ProductRecordJpo extends StageEntityJpo {
    //
    private String name;
    private String description;
    private String category;
    private String color;
    private int size;
    private String material;
    private int stockQuantity;
    private int soldQuantity;

    public ProductRecordJpo(ProductRecord productRecord) {
        //
        super(productRecord);
        BeanUtils.copyProperties(productRecord, this);
    }

    public ProductRecord toDomain() {
        //
        ProductRecord productRecord = new ProductRecord(getEntityId(), genActorKey());
        BeanUtils.copyProperties(this, productRecord);
        return productRecord;
    }

    public static List<ProductRecord> toDomains(List<ProductRecordJpo> productRecordJpos) {
        //
        return productRecordJpos.stream().map(ProductRecordJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        //
        return toJson();
    }

    public static ProductRecordJpo sample() {
        //
        return new ProductRecordJpo(ProductRecord.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
